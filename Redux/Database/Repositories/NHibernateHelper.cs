﻿using System;
using NHibernate;
namespace Redux.Database.Repositories
{
    public class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;

        public static bool BuildSessionFactory()
        {
            if (_sessionFactory != null) return false;

            try
            {
                var configuration = new NHibernate.Cfg.Configuration();
                configuration.Configure(Environment.MachineName + ".cfg.xml");
                configuration.AddAssembly(System.Reflection.Assembly.GetExecutingAssembly());
                _sessionFactory = configuration.BuildSessionFactory();
            } catch(Exception e)
            {
                Console.WriteLine("ERROR IN CONNECTION: " + e.Message);
                Console.WriteLine("Press enter to close...");
                Console.ReadLine();
                Environment.Exit(71);
            }

            return true;
        }

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    BuildSessionFactory();
                }

                return _sessionFactory;
            }
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}
