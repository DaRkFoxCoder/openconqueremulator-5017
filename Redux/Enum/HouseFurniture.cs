﻿using System;

namespace Redux.Enum
{
    public class HouseFurniture
    {
        public enum Mesh : ushort
        {
            None = 0,
            Lamp = 10,
            LowShelf = 20,
            Cabinet = 30,
            HighShelf = 40,
            BombeChest = 50,
            RosewoodCabinet = 60,
            HightCabinet = 70,
            FoldingScreen = 80,
            Dresser = 90,
            BasinRack = 101,
            Chair = 111,
            EndTable = 121,
            HorseToy = 130,
            PineCrutch = 140,
            CrutchDrawing = 150,
            Map = 160,
            ChristmasWreath = 380,
            ChristmasTree = 390
        }

        public static Mesh getMesh(uint ItemID)
        {
            HouseFurniture.Mesh furniture_mesh = HouseFurniture.Mesh.None;
            switch (ItemID)
            {
                case 721177:
                    {
                        furniture_mesh = HouseFurniture.Mesh.Lamp;
                        break;
                    }
                case 721178:
                    {
                        furniture_mesh = HouseFurniture.Mesh.LowShelf;
                        break;
                    }
                case 721179:
                    {
                        furniture_mesh = HouseFurniture.Mesh.Cabinet;
                        break;
                    }
                case 721180:
                    {
                        furniture_mesh = HouseFurniture.Mesh.HighShelf;
                        break;
                    }
                case 721181:
                    {
                        furniture_mesh = HouseFurniture.Mesh.BombeChest;
                        break;
                    }
                case 721182:
                    {
                        furniture_mesh = HouseFurniture.Mesh.RosewoodCabinet;
                        break;
                    }
                case 721183:
                    {
                        furniture_mesh = HouseFurniture.Mesh.HightCabinet;
                        break;
                    }
                case 721184:
                    {
                        furniture_mesh = HouseFurniture.Mesh.FoldingScreen;
                        break;
                    }
                case 721185:
                    {
                        furniture_mesh = HouseFurniture.Mesh.Dresser;
                        break;
                    }
                case 721186:
                    {
                        furniture_mesh = HouseFurniture.Mesh.BasinRack;
                        break;
                    }
                case 721187:
                    {
                        furniture_mesh = HouseFurniture.Mesh.Chair;
                        break;
                    }
                case 721188:
                    {
                        furniture_mesh = HouseFurniture.Mesh.EndTable;
                        break;
                    }
                case 721200:
                    {
                        furniture_mesh = HouseFurniture.Mesh.PineCrutch;
                        break;
                    }
                case 721201:
                    {
                        furniture_mesh = HouseFurniture.Mesh.PineCrutch;
                        break;
                    }
                case 721202:
                    {
                        furniture_mesh = HouseFurniture.Mesh.CrutchDrawing;
                        break;
                    }
                case 721203:
                    {
                        furniture_mesh = HouseFurniture.Mesh.Map;
                        break;
                    }
                case 720391:
                    {
                        furniture_mesh = HouseFurniture.Mesh.ChristmasTree;
                        break;
                    }
                case 720392:
                    {
                        furniture_mesh = HouseFurniture.Mesh.ChristmasWreath;
                        break;
                    }
                default:
                    {
                        furniture_mesh = HouseFurniture.Mesh.None;
                        break;
                    }
            }
            return furniture_mesh;
        }
    }
}
