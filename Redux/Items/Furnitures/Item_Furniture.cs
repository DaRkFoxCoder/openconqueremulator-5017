﻿using System;
using Redux.Game_Server;
using Redux.Structures;

namespace Redux.Items
{
    /// <summary>
    /// Handles item usage for Furniture Items
    /// </summary>
    public class Item_Furniture: IItem
	{		
        public override void Run(Player _client, ConquerItem _item) 
        {
            //Generate Packet for add Furniture in House
            var npcPacket = new Packets.Game.NpcPacket();
            npcPacket.Action = Enum.NpcEvent.LayNpc;
            npcPacket.Data = (ushort)Enum.HouseFurniture.getMesh(_item.StaticID);
            npcPacket.Type = (ushort)Enum.NpcType.RoleFurniture;
            npcPacket.UID = _item.UniqueID;
            _client.ItemUsed = _item;
            _client.Send(npcPacket);
        }
	}
}
