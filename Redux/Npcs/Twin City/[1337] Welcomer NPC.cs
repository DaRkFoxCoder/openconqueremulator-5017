﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redux.Packets.Game;
using Redux.Structures;

namespace Redux.Npcs
{

    public class NPC_1337 : INpc
    {
        /// <summary>
        /// Handles NPC usage for [1337] Welcomer
        /// </summary>
        public NPC_1337(Game_Server.Player _client)
            : base(_client)
        {
            ID = 1337;
            Face = 1;
        }

        public override void Run(Game_Server.Player _client, ushort _linkback)
        {
            Responses = new List<NpcDialogPacket>();
            AddAvatar();
            switch (_linkback)
            {
                case 0:
                    AddText("Bienvenidos a la beta abierta de OpenConquer. Todos teneis disponibles los comandos de administrador.");
                    AddText("Si ves algun fallo o alguna cosa extraña envianos un report con el comando /report asi nos ayudas a mejorar! ");
                    AddText("Novedad: 31/03/2017 - Hay muebles para provar, no abuseis o colapsareis el mapa, nuevo comando /furnitures ");
                    AddText("Novedad: 04/04/2017 - Conexión del cliente arreglada y ya se puede componer objetos n+");
                    AddOption("Lista de comandos", 1);
                    AddOption("Gracias por todo", 255);
                    break;
                case 1:
                    AddText("Lista de comandos disponibles:");
                    AddText("/exit para desconectarse, /heal para curarse, /item iditem para obtener el objeto deseado, /level 140 obtener nivel 140, /money 99999 obtener 99999 de dinero ");
                    AddText("/map mapid cordx cordy para transportarse a cualquier mapa disponible, hay mas comandos pero estos son los basicos");
                    AddOption("Cerrar", 255);
                    break;
            }
            AddFinish();
            Send();

        }
    }
}