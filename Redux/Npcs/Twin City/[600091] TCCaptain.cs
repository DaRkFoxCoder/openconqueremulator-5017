﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Redux.Packets.Game;
using Redux.Database;
using Redux.Structures;
using Redux.Enum;
using System.Threading.Tasks;

namespace Redux.Npcs
{

    public class NPC_600093: INpc
    {
        /// <summary>
        /// Coded by DaRkFox
        /// Handles NPC usage for [600093] TCCAPTAIN
        /// </summary>
        public NPC_600093(Game_Server.Player _client)
            : base(_client)
        {
            ID = 600091;
            Face = 37;
        }

        public override void Run(Game_Server.Player _client, ushort _linkback)
        {
            Responses = new List<NpcDialogPacket>();
            AddAvatar();
            switch (_linkback)
            {
                case 0:
                    {
                        if (_client.Tasks.ContainsKey(TaskType.CloudSaint) && DateTime.Now < _client.Tasks[TaskType.CloudSaint].Expires && _client.Tasks[TaskType.CloudSaint].Count >= 2)
                        {
                            AddText("I can only give you two missions a day come back tomorrow");
                            AddOption("ok", 255);
                        }
                        else if (_client.Level < 28 || _client.CloudSaintsJair_MonsterID == 1 || _client.CloudSaintsJair_MonsterID == 2 || _client.CloudSaintsJair_MonsterID == 3 || _client.CloudSaintsJair_MonsterID == 4 || _client.CloudSaintsJair_MonsterID == 5)
                        {
                            if (_client.CloudSaintsJair_MonsterID == 0)
                            {
                                AddText("Hey there i can assign you missions and if you complete them i will give you rewards!");
                                AddText("The rewards are exp and (a) Meteor!");
                                AddOption("Kill Pheasants (level 1)", 1);
                                AddOption("Kill Turtledoves (Level 7)", 2);
                                AddOption("Kill Robins (Level 12)", 3);
                                AddOption("Kill Apparitions (Level 17)", 4);
                                AddOption("Kill Poltergeists (Level 22)", 5);
                                AddOption("Maybe later i will give it a try.", 255);
                            }
                            else if (_client.CloudSaintsJair_MonsterID == 1 || _client.CloudSaintsJair_MonsterID == 2 || _client.CloudSaintsJair_MonsterID == 3 || _client.CloudSaintsJair_MonsterID == 4 || _client.CloudSaintsJair_MonsterID == 5)
                            {
                                AddText("Have you finished your mission ? Then report in");
                                AddOption("Report. Get my rewards!", 11);
                                AddOption("Abort Mission", 22);
                                AddOption("I still have a few left, be back in abit!", 255);
                            }
                        }
                        #region Map teleport dialog and check
                        else if (_client.Level >= 28 && _client.Level <= 48)
                        {
                            AddText("You have now go and continue in Phoenix Castle .");
                            AddOption("Please teleport me there.", 12);
                            AddOption("I see.", 255);
                        }
                        else if (_client.Level >= 48 && _client.Level <= 68)
                        {
                            AddText("You have now go and continue in Ape Mountain .");
                            AddOption("Please teleport me there.", 13);
                            AddOption("I see.", 255);
                        }
                        else if (_client.Level >= 68 && _client.Level <= 88)
                        {
                            AddText("You have now go and continue in Desert City .");
                            AddOption("Please teleport me there.", 14);
                            AddOption("I see.", 255);
                        }
                        else if (_client.Level >= 88 && _client.Level <= 103)
                        {
                            AddText("You have now go and continue in Bird Island .");
                            AddOption("Please teleport me there.", 15);
                            AddOption("I see.", 255);
                        }
                        else if (_client.Level >= 103 && _client.Level <= 120)
                        {
                            AddText("You have now go and continue in Mystic Castle .");
                            AddOption("Please teleport me there.", 16);
                            AddOption("I see.", 255);
                        }
                        else if (_client.Level >= 120 && _client.Level <= 130)
                        {
                            AddText("Sorry I can't help you anymore.");
                            AddOption("I see.", 255);
                        }
                        #endregion
                        break;
                    }
                case 1: //Pheasants
                    {
                        if (_client.Level < 7)
                        {
                            SetMission(_client, 1, 0, 100);
                        }
                        #region Level notify
                        else
                        {

                            if (_client.Level >= 7 && _client.Level < 12)
                            {
                                AddText("Your level is too high please choose to kill Turtledoves");
                                AddOption("Okay thanks for the advice", 0);
                            }
                            else if (_client.Level >= 12 && _client.Level < 17)
                            {
                                AddText("Your level is too high please choose to kill Robins");
                                AddOption("Okay thanks for the advice", 0);
                            }
                            else if (_client.Level >= 17 && _client.Level < 22)
                            {
                                AddText("Your level is too high please choose to kill Apparitions");
                                AddOption("Okay thanks for the advice", 0);
                            }
                            else if (_client.Level >= 22 && _client.Level < 28)
                            {
                                AddText("Your level is too high please choose to kill Poltergeists");
                                AddOption("Okay thanks for the advice", 0);
                            }
                            #endregion
                        }
                        break;
                    }
                  case 2: //turtledoves
                    if (_client.Level >= 7 && _client.Level < 12)
                    {
                        SetMission(_client, 2, 0, 100);
                    }
                    #region Level notify
                    else
                    {

                        if (_client.Level < 7)
                        {
                            AddText("Your level is too low please choose to kill Pheasants");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 12 && _client.Level < 17)
                        {
                            AddText("Your level is too high please choose to kill Robins");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 17 && _client.Level < 22)
                        {
                            AddText("Your level is too high please choose to kill Apparitions");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 22 && _client.Level < 28)
                        {
                            AddText("Your level is too high please choose to kill Poltergeists");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        #endregion
                    }
                    break;
                case 3: //robins
                    if (_client.Level >= 12 && _client.Level < 17)
                    {
                        SetMission(_client, 3, 0, 100);
                    }
                    #region Level notify
                    else
                    {

                        if (_client.Level < 7)
                        {
                            AddText("Your level is too low please choose to kill Pheasants");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        if (_client.Level >= 7 && _client.Level < 12)
                        {
                            AddText("Your level is too low please choose to kill Turtledoves");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 17 && _client.Level < 22)
                        {
                            AddText("Your level is too high please choose to kill Apparitions");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 22 && _client.Level < 28)
                        {
                            AddText("Your level is too high please choose to kill Poltergeists");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        #endregion
                    }
                    break;
                case 4: //Apparitions
                    if (_client.Level >= 17 && _client.Level < 22)
                    {
                        SetMission(_client, 4, 0, 100);
                    }
                    #region Level notify
                    else
                    {

                        if (_client.Level < 7)
                        {
                            AddText("Your level is too low please choose to kill Pheasants");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        if (_client.Level >= 7 && _client.Level < 12)
                        {
                            AddText("Your level is too low please choose to kill Turtledoves");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 12 && _client.Level < 17)
                        {
                            AddText("Your level is too low please choose to kill Robins");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 22 && _client.Level < 28)
                        {
                            AddText("Your level is too high please choose to kill Poltergeists");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        #endregion
                    }
                    break;
                case 5: //Apparitions
                    if (_client.Level >= 22 && _client.Level < 28)
                    {
                        SetMission(_client, 5, 0, 100);
                    }
                    #region Level notify
                    else
                    {

                        if (_client.Level < 7)
                        {
                            AddText("Your level is too low please choose to kill Pheasants");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        if (_client.Level >= 7 && _client.Level < 12)
                        {
                            AddText("Your level is too low please choose to kill Turtledoves");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 12 && _client.Level < 17)
                        {
                            AddText("Your level is too low please choose to kill Robins");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        else if (_client.Level >= 17 && _client.Level < 22)
                        {
                            AddText("Your level is too low please choose to kill Apparitions");
                            AddOption("Okay thanks for the advice", 0);
                        }
                        #endregion
                    }
                    break;
                #region Reward
                case 11:
                    if (_client.CloudSaintsJair_MonsterCount == _client.CloudSaintsJair_MonsterKills)
                    {
                        _client.CloudSaintsJair_MonsterID = 0;
                        _client.CloudSaintsJair_MonsterKills = 0;
                        _client.CloudSaintsJair_MonsterCount = 0;
                        _client.GainExpBall(100);
                        _client.CreateItem(Constants.METEOR_ID);
                        //_client.SelfEffect("angelwing");
                        _client.Save();
                        _client.SendMessage("Mission has been reported, And you have recieved (a) Meteor.", ChatType.GM);

                        if (!_client.Tasks.ContainsKey(TaskType.CloudSaint))
                        {
                            _client.Tasks.TryAdd(TaskType.CloudSaint, new Structures.Task(_client.UID, TaskType.CloudSaint, DateTime.Now.AddHours(24)));

                        }
                        var task = _client.Tasks[TaskType.CloudSaint];
                        if (DateTime.Now > task.Expires)
                        {
                            task.Expires = DateTime.Now.AddHours(24); task.Count = 0;
                        }
                        if (task.Count >= 2)
                        {

                        }
                        else
                        {
                            _client.Tasks[TaskType.CloudSaint].Count++;

                        }
                    }
                    else
                    {
                        var monster = ServerDatabase.Context.Monstertype.GetById(_client.CloudSaintsJair_MonsterID);
                        AddText("You have to kill " + (_client.CloudSaintsJair_MonsterCount - _client.CloudSaintsJair_MonsterKills) + " " + monster.Name);
                        AddOption("Okay ill get to it", 255);
                    }
                    break;
                #endregion
                #region Mission Abort
                case 22:
                    AddText("Are you sure you want to abort mission? You will loose all progress");
                    AddOption("Yes", 222);
                    AddOption("No", 255);
                    break;
                case 222:
                    _client.CloudSaintsJair_MonsterID = 0;
                    _client.CloudSaintsJair_MonsterKills = 0;
                    _client.CloudSaintsJair_MonsterCount = 0;
                    _client.Save();
                    _client.SendMessage("Mission has been Aborted.", Enum.ChatType.GM);
                    break;
                #endregion
                #region Teleport to location
                case 12:
                    _client.ChangeMap(1011);
                    break;
                case 13:
                    _client.ChangeMap(1020);
                    break;
                case 14:
                    _client.ChangeMap(1000);
                    break;
                case 15:
                    _client.ChangeMap(1015);
                    break;
                case 16:
                    _client.ChangeMap(1000, 85, 321);
                    break;
                    #endregion*/
            }

            AddFinish();
            Send();
        }
        #region Mission Setter
        public void SetMission(Game_Server.Player _client, ushort _MonsterID, ushort _MonsterKills, ushort _MonsterCount)
        {
            _client.CloudSaintsJair_MonsterID = _MonsterID;
            _client.CloudSaintsJair_MonsterKills = _MonsterKills;
            _client.CloudSaintsJair_MonsterCount = _MonsterCount;
            var monster = ServerDatabase.Context.Monstertype.GetById(_MonsterID);
            var info = Database.ServerDatabase.Context.ItemInformation.GetById(750000);
            var item = new ConquerItem((uint)Common.ItemGenerator.Counter, info);
            item.SetOwner(_client);
            item.Durability = _MonsterCount;
            item.MaximumDurability = _MonsterID;
            if (_client.AddItem(item)) { _client.Send(ItemInformationPacket.Create(item)); };
            item.Save();
            _client.Save();
        }
        #endregion
    }
}