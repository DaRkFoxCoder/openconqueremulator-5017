# OpenConquerEmulator
This is an open-source implementation of Conquer Online server based on pro4never's Redux V2 source code. Base Version 5017.

Unique Feature: Multiversion => 5017 or 5072

## **The project has been discontinued but here are the code available for all ** ##

# Open Beta Server For Testing
Is open beta server for public testing
You can enter in server if you download the client 5065 and use a [ConquerLoader](https://mega.nz/#!8UgVELQC!wHQptm1_eXnE4vKN0JJUyDnIvA3inqxa1TZjNRSQtjg) with this [configuration file](https://mega.nz/#!4RwHnbiT!t5yTSSSohc3HkwYZKAgt-XYbx-nd06EDAuRk_EE5MbE)

Register your account [here!](http://testingco.ddns.net/)
**
[Server is closed now]**

# How do I use this source?!
https://www.youtube.com/watch?v=Yb_zBSnvM2Y

Setup is incredibly simple but you will need to ensure you have a proper version of mysql installed. Follow the steps below and you'll be running within minutes!

- Download Mysql: http://dev.mysql.com/downloads/installer/5.6.html
- Download the Source
- Execute the SQL backup (using any management software such as navicat)
- Create an Account in the database (using any mangement software such as navicat (Permission = 5 for Admin)
- Run the source
- Enter the Server Information requested on first run (ip/name/db info)

NOTE: Use network, hamachi or external ip. 127.0.0.1 will not work.

NOTE: If you bluescreen trying to run conquer, remove the tq anti virus folder and you will be fine.

If you do not have a client already you will need to follow the next steps

- Download a 5065 client. (remember to delete tq antivirus folder, It's the strange named folder)
- Download [ConquerLoader](https://mega.nz/#!8UgVELQC!wHQptm1_eXnE4vKN0JJUyDnIvA3inqxa1TZjNRSQtjg): (Credits to nullable)
- Unrar the loader into the 5065 client. Edit IP inside LoaderSet.ini

TADA! You're ready to play

# Original Source (pro4never):
http://www.elitepvpers.com/forum/co2-pserver-guides-releases/2692305-redux-v2-official-5065-classic-source.html

# Special Thanks to:
- pro4never: Original Creator.
- Many, many, many contributors from elitepvpers. See Original Source.